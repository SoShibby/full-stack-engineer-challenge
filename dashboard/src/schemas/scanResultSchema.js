export default {
  title: 'Scan Result Registration Form',
  type: 'object',
  properties: {
    status: { type: 'string', enum: ['Queued', 'In Progress', 'Success', 'Failure'], title: 'Status' },
    repositoryName: { type: 'string', title: 'Repository Name' },
    queuedAt: { type: 'string', format: 'date-time', title: 'Queued At' },
    scanningAt: { type: 'string', format: 'date-time', title: 'Scanning At' },
    finishedAt: { type: 'string', format: 'date-time', title: 'Finished At' },
    findings: {
      title: 'Findings',
      type: 'array',
      items: {
        type: 'object',
        title: 'Finding',
        properties: {
          type: { type: 'string', title: 'Type' },
          ruleId: { type: 'string', title: 'Rule Id' },
          location: {
            title: 'Location',
            type: 'object',
            properties: {
              path: { type: 'string', title: 'Path' },
              positions: {
                title: 'Position',
                type: 'object',
                properties: {
                  begin: {
                    title: 'Begin',
                    type: 'object',
                    properties: {
                      line: { type: 'number', title: 'Line Number' },
                    },
                    required: [
                      'line',
                    ],
                  },
                },
                required: [
                  'begin',
                ],
              },
            },
            required: [
              'path',
              'positions',
            ],
          },
          metadata: {
            title: 'Metadata',
            type: 'object',
            properties: {
              description: { type: 'string', title: 'Description' },
              severity: { type: 'string', title: 'Severity', enum: ['LOW', 'MEDIUM', 'HIGH'] }, // Can we have anything else than LOW, MEDIUM and HIGH?
            },
            required: [
              'description',
              'severity',
            ],
          },
        },
        required: [
          'type',
          'ruleId',
          'location',
          'metadata',
        ],
      },
    },
  },
  required: [
    'status',
    'repositoryName',
    'queuedAt',
    'scanningAt',
    'finishedAt',
  ],
  additionalProperties: false,
};
