import React from 'react';
import renderer from 'react-test-renderer';
import { ThemeProvider } from '@material-ui/styles';
import Findings from '../findings';
import theme from '../../../theme';

it('renders correctly', () => {
  const params = {
    match: {
      params: {
        id: 'test',
      },
    },
  };

  const tree = renderer
    .create(
      <ThemeProvider theme={theme}>
        <Findings {...params} />
      </ThemeProvider>,
    ).toJSON();

  expect(tree).toMatchSnapshot();
});
