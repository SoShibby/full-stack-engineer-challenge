import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { useScanResult } from '../../services';
import { FindingsTable, Header } from './components';
import { Spinner, Alert } from '../../components';
import ResponseErrorUtil from '../../utils/responseErrorUtil';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3),
  },
  content: {
    marginTop: theme.spacing(2),
  },
}));

const Findings = (props) => {
  const classes = useStyles();
  const [{ data, loading, error }] = useScanResult(props.match.params.id);

  return (
    <div className={classes.root}>
      <div className={classes.content}>
        <Spinner isLoading={loading}>
          <Alert
            visible={!!error}
            header="Failed to fetch scan result"
            message={ResponseErrorUtil.humanReadable(error)}
          >
            <Header scanResult={data && data.result} />
            <FindingsTable scanResult={data && data.result} />
          </Alert>
        </Spinner>
      </div>
    </div>
  );
};

export default Findings;
