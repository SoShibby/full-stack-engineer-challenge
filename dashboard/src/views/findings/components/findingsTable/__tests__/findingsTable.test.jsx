import React from 'react';
import renderer from 'react-test-renderer';
import FindingsTable from '../findingsTable';
import scanResult from '../../../../../examples/scan-result.json';

it('renders correctly', () => {
  const tree = renderer
    .create(
      <FindingsTable scanResult={scanResult} />,
    ).toJSON();

  expect(tree).toMatchSnapshot();
});
