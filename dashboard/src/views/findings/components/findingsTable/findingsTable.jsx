import React from 'react';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardContent,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from '@material-ui/core';

const useStyles = makeStyles(() => ({
  content: {
    padding: 0,
  },
  inner: {
    minWidth: 1050,
  },
}));

const ScanResultsTable = (props) => {
  const { scanResult } = props;
  const classes = useStyles();

  return (
    <Card>
      <CardContent className={classes.content}>
        <div className={classes.inner}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Rule Id</TableCell>
                <TableCell>Description</TableCell>
                <TableCell>Severity</TableCell>
                <TableCell>Path Name</TableCell>
                <TableCell>Line Number</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {scanResult.findings.map((finding) => (
                <TableRow
                  hover
                  key={scanResult._id}
                >
                  <TableCell>{finding.ruleId}</TableCell>
                  <TableCell>{finding.metadata.description}</TableCell>
                  <TableCell>{finding.metadata.severity}</TableCell>
                  <TableCell>{finding.location.path}</TableCell>
                  <TableCell>{finding.location.positions.begin.line}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </div>
      </CardContent>
    </Card>
  );
};

export default ScanResultsTable;
