import React from 'react';
import renderer from 'react-test-renderer';
import Header from '../header';
import scanResult from '../../../../../examples/scan-result.json';

it('renders correctly', () => {
  const tree = renderer
    .create(
      <Header scanResult={scanResult} />,
    ).toJSON();

  expect(tree).toMatchSnapshot();
});
