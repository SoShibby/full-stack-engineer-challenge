import React from 'react';

const Header = (props) => {
  const { scanResult } = props;

  return (
    <h2>
      Security issues found in {scanResult.repositoryName}
    </h2>
  );
};

export default Header;
