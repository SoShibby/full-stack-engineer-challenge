import React from 'react';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardContent,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from '@material-ui/core';
import ScanResultRow from './scanResultsTableRow';

const useStyles = makeStyles(() => ({
  content: {
    padding: 0,
  },
  inner: {
    minWidth: 1050,
  },
}));

const ScanResultsTable = (props) => {
  const { scanResults } = props;
  const classes = useStyles();

  return (
    <Card>
      <CardContent className={classes.content}>
        <div className={classes.inner}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Repository</TableCell>
                <TableCell>Status</TableCell>
                <TableCell>Finished At</TableCell>
                <TableCell>Queued At</TableCell>
                <TableCell>Scanning At</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {scanResults.map((scanResult) => (
                <ScanResultRow key={scanResult._id} scanResult={scanResult} />
              ))}
            </TableBody>
          </Table>
        </div>
      </CardContent>
    </Card>
  );
};

export default ScanResultsTable;
