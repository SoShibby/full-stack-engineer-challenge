import React from 'react';
import { withRouter } from 'react-router-dom';
import { makeStyles } from '@material-ui/styles';
import {
  TableRow,
  TableCell,
  Typography,
} from '@material-ui/core';
import Badge from '@material-ui/core/Badge';
import moment from 'moment';

const useStyles = makeStyles(() => ({
  row: {
    cursor: 'pointer',
  },
}));

const Row = (props) => {
  const { scanResult, history } = props;
  const classes = useStyles();

  const onRowClick = (scanResult) => {
    props.history.push(`/findings/${scanResult._id}`);
  };

  return (
    <TableRow
      hover
      className={classes.row}
      onClick={() => onRowClick(scanResult)}
      key={scanResult._id}
    >
      <TableCell>{scanResult.repositoryName}</TableCell>
      <TableCell>
        {scanResult.findings.length > 0
          ? (
            <Badge color="secondary" badgeContent={scanResult.findings.length} className={classes.margin}>
              <Typography className={classes.padding}>{scanResult.status}</Typography>
            </Badge>
          )
          : <Typography className={classes.padding}>{scanResult.status}</Typography>}
      </TableCell>
      <TableCell>
        {moment(scanResult.finishedAt).fromNow()}
      </TableCell>
      <TableCell>
        {moment(scanResult.queuedAt).fromNow()}
      </TableCell>
      <TableCell>
        {moment(scanResult.scanningAt).fromNow()}
      </TableCell>
    </TableRow>
  );
}

export default withRouter(Row);
