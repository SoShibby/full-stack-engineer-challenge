import React from 'react';
import renderer from 'react-test-renderer';
import { BrowserRouter } from 'react-router-dom';
import ScanResultsTable from '../scanResultsTable';
import scanResult from '../../../../../examples/scan-result.json';

it('renders correctly', () => {
  const tree = renderer
    .create(
      <BrowserRouter>
        <ScanResultsTable scanResults={[scanResult]} />
      </BrowserRouter>,
    ).toJSON();

  expect(tree).toMatchSnapshot();
});
