import React from 'react';
import renderer from 'react-test-renderer';
import { BrowserRouter } from 'react-router-dom';
import ScanResultsTableRow from '../scanResultsTableRow';
import scanResult from '../../../../../examples/scan-result.json';

it('renders correctly', () => {
  const tree = renderer
    .create(
      <BrowserRouter>
        <ScanResultsTableRow scanResult={scanResult} />
      </BrowserRouter>,
    ).toJSON();

  expect(tree).toMatchSnapshot();
});
