import React from 'react';
import renderer from 'react-test-renderer';
import { ThemeProvider } from '@material-ui/styles';
import ScanResultActions from '../scanResultActions';
import theme from '../../../../../theme';

it('renders correctly', () => {
  const tree = renderer
    .create(
      <ThemeProvider theme={theme}>
        <ScanResultActions />
      </ThemeProvider>,
    ).toJSON();

  expect(tree).toMatchSnapshot();
});
