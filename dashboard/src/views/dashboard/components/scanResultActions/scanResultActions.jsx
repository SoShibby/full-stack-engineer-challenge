import React, { useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import { Button } from '@material-ui/core';
import AddScanResult from '../addScanResult';

const useStyles = makeStyles((theme) => ({
  row: {
    height: '42px',
    display: 'flex',
    alignItems: 'center',
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(2),
  },
  spacer: {
    flexGrow: 1,
  },
}));

const ScanResultActions = (props) => {
  const [dialogVisiblity, setDialogVisibility] = useState(false);
  const classes = useStyles();

  const onDialogClose = () => {
    setDialogVisibility(false);
    props.onUpdate();
  };

  return (
    <div>
      <div className={classes.row}>
        <span className={classes.spacer} />
        <Button
          onClick={() => setDialogVisibility(true)}
          color="primary"
          variant="contained"
        >
          Add scan result
        </Button>
        <AddScanResult open={dialogVisiblity} onClose={onDialogClose} />
      </div>
    </div>
  );
};

export default ScanResultActions;
