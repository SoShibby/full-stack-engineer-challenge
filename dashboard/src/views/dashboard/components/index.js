import ScanResultsTable from './scanResultsTable';
import ScanResultActions from './scanResultActions';

export {
  ScanResultsTable,
  ScanResultActions,
};
