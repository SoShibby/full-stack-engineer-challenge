import React from 'react';
import renderer from 'react-test-renderer';
import AddScanResult from '../addScanResult';

it('renders correctly', () => {
  const tree = renderer
    .create(
      <AddScanResult open={false} />,
    ).toJSON();

  expect(tree).toMatchSnapshot();
});
