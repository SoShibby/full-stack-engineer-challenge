import React, { useEffect } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import MuiForm from 'rjsf-material-ui';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import { SnackBar } from '../../../../components';
import { usePostScanResult } from '../../../../services';
import schema from '../../../../schemas/scanResultSchema';
import logger from '../../../../logger';
import ResponseErrorUtil from '../../../../utils/responseErrorUtil';

export default function FormDialog(props) {
  const [showDialog, setShowDialog] = React.useState(props.open);
  const [showSnackBar, setShowSnackBar] = React.useState(false);
  const [snackBarMessage, setSnackBarMessage] = React.useState('');
  const [snackBarVariant, setSnackBarVariant] = React.useState('info');
  const formData = {};
  const [{ loading, error }, executePost] = usePostScanResult()

  useEffect(() => {
    setShowDialog(props.open);
  }, [props.open]);

  useEffect(() => {
    if (error) {
      setSnackBarMessage(`Failed to create scan result. ${ResponseErrorUtil.humanReadable(error)}`);
      setSnackBarVariant('error');
      setShowSnackBar(true);
    }
  }, [error]);

  const onSubmit = (event) => {
    executePost({
      data: event.formData,
    }).then(() => {
      logger.info('Successfully created scan result.');
      props.onClose();
    });
  };

  const onCancel = () => {
    props.onClose();
  };

  const onCloseSnackBar = () => {
    setShowSnackBar(false);
  };

  return (
    <Dialog fullWidth={true} open={showDialog} onClose={onCancel} aria-labelledby="form-dialog-title">
      <DialogContent>
        <MuiForm
          schema={schema}
          formData={formData}
          onCancel={onCancel}
          onSubmit={onSubmit}
        >
          <Box mt={2}>
            <Button
              variant="contained"
              color="default"
              onClick={onCancel}
            >
              Cancel
            </Button>
            <Button
              variant="contained"
              color="primary"
              type="submit"
            >
              Submit
            </Button>
          </Box>
        </MuiForm>
      </DialogContent>
      <SnackBar
        open={showSnackBar}
        message={snackBarMessage}
        variant={snackBarVariant}
        onClose={onCloseSnackBar}
      />
    </Dialog>
  );
}
