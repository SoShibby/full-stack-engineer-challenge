import React from 'react';
import renderer from 'react-test-renderer';
import { ThemeProvider } from '@material-ui/styles';
import Dashboard from '../dashboard';
import theme from '../../../theme';

it('renders correctly', () => {
  const tree = renderer
    .create(
      <ThemeProvider theme={theme}>
        <Dashboard />
      </ThemeProvider>,
    ).toJSON();

  expect(tree).toMatchSnapshot();
});
