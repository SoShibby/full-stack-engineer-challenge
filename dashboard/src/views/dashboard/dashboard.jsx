import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { ScanResultsTable, ScanResultActions } from './components';
import { useScanResults } from '../../services';
import { Spinner, Alert } from '../../components';
import ResponseErrorUtil from '../../utils/responseErrorUtil';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3),
  },
  content: {
    marginTop: theme.spacing(2),
  },
}));

const Dashboard = () => {
  const classes = useStyles();
  const [{ data, loading, error }, refetch] = useScanResults();

  return (
    <div className={classes.root}>
      <div className={classes.content}>
        <ScanResultActions onUpdate={refetch} />
        <Spinner isLoading={loading}>
          <Alert
            visible={!!error}
            header="Failed to fetch scan results"
            message={ResponseErrorUtil.humanReadable(error)}
          >
            <ScanResultsTable scanResults={data && data.result} />
          </Alert>
        </Spinner>
      </div>
    </div>
  );
};

export default Dashboard;
