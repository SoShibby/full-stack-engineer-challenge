import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3),
  },
  content: {
    padding: theme.spacing(1),
    textAlign: 'center',
    maxWidth: 800,
  },
}));


const Home = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Paper className={classes.content}>
        <h2>Welcome!</h2>
        <p>Use the sidebar to the left to navigate around the site.</p>
      </Paper>
    </div>
  );
};

export default Home;
