import React from 'react';
import renderer from 'react-test-renderer';
import { ThemeProvider } from '@material-ui/styles';
import SnackBar from '../snackBar';
import theme from '../../../theme';

it('renders correctly', () => {
  const tree = renderer
    .create(
      <ThemeProvider theme={theme}>
        <SnackBar />
      </ThemeProvider>,
    ).toJSON();

  expect(tree).toMatchSnapshot();
});
