import React from 'react';
import LinearProgress from '@material-ui/core/LinearProgress';

const Spinner = (props) => {
  const { children, isLoading } = props;

  return (
    <>
      {
        isLoading ? <LinearProgress /> : children
      }
    </>
  );
};

export default Spinner;
