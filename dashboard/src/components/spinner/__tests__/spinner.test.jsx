import React from 'react';
import renderer from 'react-test-renderer';
import { ThemeProvider } from '@material-ui/styles';
import Spinner from '../spinner';
import theme from '../../../theme';

it('renders correctly', () => {
  const tree = renderer
    .create(
      <ThemeProvider theme={theme}>
        <Spinner />
      </ThemeProvider>,
    ).toJSON();

  expect(tree).toMatchSnapshot();
});
