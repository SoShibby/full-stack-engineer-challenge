import React from 'react';
import renderer from 'react-test-renderer';
import { ThemeProvider } from '@material-ui/styles';
import Alert from '../alert';
import theme from '../../../theme';

it('renders correctly', () => {
  const tree = renderer
    .create(
      <ThemeProvider theme={theme}>
        <Alert />
      </ThemeProvider>,
    ).toJSON();

  expect(tree).toMatchSnapshot();
});
