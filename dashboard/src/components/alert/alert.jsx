import React from 'react';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles((theme) => ({
  alert: {
    padding: theme.spacing(3),
    marginLeft: 'auto',
    marginRight: 'auto',
    width: 600,
  },
}));

const Error = (props) => {
  const classes = useStyles();
  const {
    visible,
    header,
    message,
    children,
  } = props;

  return (
    <>
      {
        visible
          ? (
            <Paper className={classes.alert}>
              <Typography variant="h6" component="h5">{header}</Typography>
              <Typography component="p">{message}</Typography>
            </Paper>
          )
          : children
      }
    </>
  );
};

export default Error;
