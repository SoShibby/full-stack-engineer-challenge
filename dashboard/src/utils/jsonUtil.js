const prettyPrint = (json) => JSON.stringify(json, null, 2);

export {
  // eslint-disable-next-line import/prefer-default-export
  prettyPrint,
};
