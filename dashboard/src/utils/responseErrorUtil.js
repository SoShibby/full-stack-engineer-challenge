const ResponseErrorUtil = {
  humanReadable: (error) => {
    if (!error) {
      return 'An unknown error occured, please try again later';
    }

    if (!error.response) {
      return 'Connection error, please try again later';
    }

    if (!error.response.data
      || !error.response.data.error
      || !error.response.data.error.message) {
      return 'An unknown error occured, please try again later';
    }

    return error.response.data.error.message;
  },
};

export default ResponseErrorUtil;
