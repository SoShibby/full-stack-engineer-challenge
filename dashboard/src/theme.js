import { createMuiTheme } from '@material-ui/core';

const theme = createMuiTheme({
  overrides: {
    MuiPaper: {
      root: {
      },
    },
    MuiIconButton: {
      root: {
        padding: 0,
      },
    },
  },
});

export default theme;
