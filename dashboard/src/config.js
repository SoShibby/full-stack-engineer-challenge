import nconf from 'nconf';
import { prettyPrint } from './utils/jsonUtil';

nconf.env({
  parseValues: true,
  lowerCase: true,
  separator: '__',
  whitelist: [
    'react_app_scan_result_service__host',
    'react_app_scan_result_service__base_path',
  ],
});

nconf.prettyPrint = () => prettyPrint(nconf.get());

export default nconf;
