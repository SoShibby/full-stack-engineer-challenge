import winston from 'winston';
import config from './config';

const {
  printf,
  combine,
  timestamp,
} = winston.format;

// Create a logger that outputs to the console.
const logger = winston.createLogger({
  level: config.get('log:level'),
  transports: [new winston.transports.Console()],
});

const stringFormat = printf((log) => `${log.timestamp} [${log.level}]: ${log.message}`);

logger.format = combine(
  timestamp(),
  stringFormat,
);

export default logger;
