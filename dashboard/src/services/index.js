import {
  useScanResults,
  useScanResult,
  usePostScanResult,
} from './scanResultService';

export {
  useScanResults,
  useScanResult,
  usePostScanResult,
};
