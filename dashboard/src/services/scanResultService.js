import useAxios from 'axios-hooks';
import config from '../config';

const host = config.get('react_app_scan_result_service:host') || `${window.location.protocol}//${window.location.hostname}`;
const port = config.get('react_app_scan_result_service:port') || 8090;
const basePath = config.get('react_app_scan_result_service:base_path') || '';
const baseUrl = `${host}:${port}${basePath}`;

const useScanResults = () => useAxios(`${baseUrl}/api/v1/scan-results`);
const useScanResult = (id) => useAxios(`${baseUrl}/api/v1/scan-results/${id}`);
const usePostScanResult = () => useAxios(
  {
    url: `${baseUrl}/api/v1/scan-results`,
    method: 'POST',
  },
  {
    manual: true,
  },
);

export {
  useScanResults,
  useScanResult,
  usePostScanResult,
};
