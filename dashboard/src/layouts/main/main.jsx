import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Sidebar, Topbar } from './components';

const useStyles = makeStyles(() => ({
  root: {
    paddingTop: 64,
    height: '100%',
  },
  shiftContent: {
    paddingLeft: 240,
  },
  content: {
    height: '100%',
  },
}));

const Main = (props) => {
  const { children } = props;

  const classes = useStyles();

  return (
    <div className={`${classes.root} ${classes.shiftContent}`}>
      <Topbar />
      <Sidebar />
      <main className={classes.content}>
        {children}
      </main>
    </div>
  );
};

export default Main;
