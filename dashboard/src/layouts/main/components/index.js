import Topbar from './topbar';
import Sidebar from './sidebar';

export {
  // eslint-disable-next-line import/prefer-default-export
  Topbar,
  Sidebar,
};
