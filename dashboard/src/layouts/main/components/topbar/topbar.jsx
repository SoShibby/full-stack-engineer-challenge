import React from 'react';
import { AppBar, Toolbar } from '@material-ui/core';
import { Link as RouterLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(() => ({
  name: {
    fontWeight: 'bold',
    fontSize: 'large',
    marginLeft: 15,
  },
}));

const Topbar = () => {
  const classes = useStyles();

  return (
    <AppBar>
      <Toolbar>
        <RouterLink to="/">
          <img
            alt="Logo"
            src="/logo-guardrails.svg"
          />
        </RouterLink>
        <span className={classes.name}>GUARDRAILS</span>
      </Toolbar>
    </AppBar>
  );
}

export default Topbar;
