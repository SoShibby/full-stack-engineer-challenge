import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Drawer } from '@material-ui/core';
import HomeIcon from '@material-ui/icons/Home';
import DashboardIcon from '@material-ui/icons/Dashboard';
import SidebarNav from './sidebarNav';

const useStyles = makeStyles(() => ({
  drawer: {
    width: 240,
    marginTop: 64,
  },
}));

const Sidebar = () => {
  const classes = useStyles();

  const pages = [
    {
      title: 'Home',
      href: '/',
      icon: <HomeIcon />,
    },
    {
      title: 'Dashboard',
      href: '/dashboard',
      icon: <DashboardIcon />,
    },
  ];

  return (
    <Drawer
      anchor="left"
      classes={{ paper: classes.drawer }}
      open
      variant="persistent"
    >
      <div>
        <SidebarNav
          pages={pages}
        />
      </div>
    </Drawer>
  );
};

export default Sidebar;
