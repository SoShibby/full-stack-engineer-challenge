import React from 'react';
import renderer from 'react-test-renderer';
import { ThemeProvider } from '@material-ui/styles';
import { BrowserRouter } from 'react-router-dom';
import Sidebar from '../sidebar';
import theme from '../../../../../theme';

it('renders correctly', () => {
  const tree = renderer
    .create(
      <BrowserRouter>
        <ThemeProvider theme={theme}>
          <Sidebar />
        </ThemeProvider>
      </BrowserRouter>,
    ).toJSON();

  expect(tree).toMatchSnapshot();
});
