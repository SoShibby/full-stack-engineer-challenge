import React from 'react';
import { Switch } from 'react-router-dom';
import { RouteWithLayout } from './components';
import { MainLayout } from './layouts';
import { Home, Dashboard, Findings } from './views';

const Routes = () => (
  <Switch>
    <RouteWithLayout
      component={Home}
      layout={MainLayout}
      path="/"
      exact
    />
    <RouteWithLayout
      component={Dashboard}
      layout={MainLayout}
      path="/dashboard"
      exact
    />
    <RouteWithLayout
      component={Findings}
      layout={MainLayout}
      path="/findings/:id"
      exact
    />
  </Switch>
);

export default Routes;
