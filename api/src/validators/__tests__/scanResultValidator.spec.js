import validator from '../scanResultValidator';

const { describe, test, expect } = global;
const { arrayContaining } = expect;

const validScanResult = {
  id: '123',
  status: 'Queued',
  repositoryName: 'RepoA',
  queuedAt: '2015-06-22T13:17:21+0000',
  scanningAt: '2015-06-22T13:17:21+0000',
  finishedAt: '2015-06-22T13:17:21+0000',
  findings: [
    {
      type: 'sast',
      ruleId: 'G402',
      location: {
        path: 'connectors/apigateway.go',
        positions: {
          begin: {
            line: 60,
          },
        },
      },
      metadata: {
        description: 'TLS InsecureSkipVerify set true.',
        severity: 'HIGH',
      },
    },
    {
      type: 'sast',
      ruleId: 'G404',
      location: {
        path: 'util/util.go',
        positions: {
          begin: {
            line: 32,
          },
        },
      },
      metadata: {
        description: 'Use of weak random number generator (math/rand instead of crypto/rand)',
        severity: 'HIGH',
      },
    },
  ],
};

const deepCopy = (obj) => JSON.parse(JSON.stringify(obj));
const getErrorStacks = (result) => result.errors.map((error) => error.stack);

describe('scanResultValidator', () => {
  test('valid scan result should pass validation', async () => {
    await validator.validate(validScanResult);
  });

  test('scan result of type null should fail validaton', async () => {
    const scanResult = null;

    expect.hasAssertions();

    try {
      await validator.validate(scanResult);
    } catch (error) {
      expect(Object.keys(error.errors).length).toBe(1);
      expect(error.errors.repositoryName.message).toBe('Path `repositoryName` is required.');
    }
  });

  test('empty scan result object should fail validaton', async () => {
    const scanResult = {};

    expect.hasAssertions();

    try {
      await validator.validate(scanResult);
    } catch (error) {
      expect(Object.keys(error.errors).length).toBe(1);
      expect(error.errors.repositoryName.message).toBe('Path `repositoryName` is required.');
    }
  });

  test('not specifing required scanResult.findings properties should fail validaton', async () => {
    const scanResult = deepCopy(validScanResult);
    scanResult.findings = [{}];

    expect.hasAssertions();

    try {
      await validator.validate(scanResult);
    } catch (error) {
      expect(Object.keys(error.errors).length).toBe(4);
      expect(error.errors['findings.0.type'].message).toBe('Path `type` is required.');
      expect(error.errors['findings.0.ruleId'].message).toBe('Path `ruleId` is required.');
      expect(error.errors['findings.0.location'].message).toBe('Path `location` is required.');
      expect(error.errors['findings.0.metadata'].message).toBe('Path `metadata` is required.');
    }
  });

  test('not specifing required scanResult.findings.location properties should fail validaton', async () => {
    const scanResult = deepCopy(validScanResult);
    scanResult.findings[0].location = {};

    expect.hasAssertions();

    try {
      await validator.validate(scanResult);
    } catch (error) {
      expect(Object.keys(error.errors).length).toBe(3);
      expect(error.errors['findings.0.location.positions'].message).toBe('Path `positions` is required.');
      expect(error.errors['findings.0.location.path'].message).toBe('Path `path` is required.');
      expect(error.errors['findings.0.location'].errors.positions.message).toBe('Path `positions` is required.');
      expect(error.errors['findings.0.location'].errors.path.message).toBe('Path `path` is required.');
    }
  });

  test('not specifing required scanResult.findings.location.positions properties should fail validaton', async () => {
    const scanResult = deepCopy(validScanResult);
    scanResult.findings[0].location.positions = {};

    expect.hasAssertions();

    try {
      await validator.validate(scanResult);
    } catch (error) {
      expect(Object.keys(error.errors).length).toBe(3);
      expect(error.errors['findings.0.location.positions.begin'].message).toBe('Path `begin` is required.');
      expect(error.errors['findings.0.location.positions'].message).toBe('Validation failed: begin: Path `begin` is required.');
      expect(error.errors['findings.0.location'].message).toBe('Validation failed: positions.begin: Path `begin` is required., positions: Validation failed: begin: Path `begin` is required.');
    }
  });

  test('not specifing required scanResult.findings.location.positions.begin properties should fail validaton', async () => {
    const scanResult = deepCopy(validScanResult);
    scanResult.findings[0].location.positions.begin = {};

    expect.hasAssertions();

    try {
      await validator.validate(scanResult);
    } catch (error) {
      expect(Object.keys(error.errors).length).toBe(4);
      expect(error.errors['findings.0.location'].message).toBe('Validation failed: positions.begin.line: Path `line` is required., positions.begin: Validation failed: line: Path `line` is required., positions: Validation failed: begin.line: Path `line` is required., begin: Validation failed: line: Path `line` is required.');

      expect(error.errors['findings.0.location.positions'].message).toBe('Validation failed: begin.line: Path `line` is required., begin: Validation failed: line: Path `line` is required.');

      expect(error.errors['findings.0.location.positions.begin'].message).toBe('Validation failed: line: Path `line` is required.');

      expect(error.errors['findings.0.location.positions.begin.line'].message).toBe('Path `line` is required.');
    }
  });

  test('not specifing required scanResult.metadata properties should fail validaton', async () => {
    const scanResult = deepCopy(validScanResult);
    scanResult.findings[0].metadata = {};

    expect.hasAssertions();

    try {
      await validator.validate(scanResult);
    } catch (error) {
      expect(Object.keys(error.errors).length).toBe(3);
      expect(error.errors['findings.0.metadata'].message).toBe('Validation failed: severity: Path `severity` is required., description: Path `description` is required.');
      expect(error.errors['findings.0.metadata.description'].message).toBe('Path `description` is required.');
      expect(error.errors['findings.0.metadata.severity'].message).toBe('Path `severity` is required.');
    }
  });

  test('scan result severity must be one of LOW,MEDIUM,HIGH', async () => {
    const scanResult = deepCopy(validScanResult);
    scanResult.findings[0].metadata = {
      severity: 'INVALID',
      description: 'test',
    };

    expect.hasAssertions();

    try {
      await validator.validate(scanResult);
    } catch (error) {
      expect(Object.keys(error.errors).length).toBe(2);
      expect(error.errors['findings.0.metadata'].message).toBe('Validation failed: severity: `INVALID` is not a valid enum value for path `severity`.');
      expect(error.errors['findings.0.metadata.severity'].message).toBe('`INVALID` is not a valid enum value for path `severity`.');
    }
  });

  test('scan result status must be one of Queued, In Progress, Success, Failure', async () => {
    const scanResult = deepCopy(validScanResult);
    scanResult.status = 'INVALID';

    expect.hasAssertions();

    try {
      await validator.validate(scanResult);
    } catch (error) {
      expect(Object.keys(error.errors).length).toBe(1);
      expect(error.errors.status.message).toBe('`INVALID` is not a valid enum value for path `status`.');
    }
  });
});
