import ScanResultModel from '../database/models/scanResultModel';

export default {
  validate: (obj) => new ScanResultModel(obj).validate(),
};
