import { Router } from 'express';
import { async } from './middlewares';
import {
  noRouteFoundController,
  scanResultController,
  healthController,
  metricsController,
} from './controllers';

export default (app) => {
  const router = Router();

  /* Scan Results */
  router.route('/api/v1/scan-results').get(async(scanResultController.getScanResults));
  router.route('/api/v1/scan-results/:id').get(async(scanResultController.getScanResult));
  router.route('/api/v1/scan-results').post(async(scanResultController.postScanResult));

  /* Metrics */
  router.route('/metrics').get(metricsController.getMetrics);

  /* Health */
  router.route('/live').get(healthController.live());
  router.route('/ready').get(healthController.ready());
  router.route('/health').get(healthController.health());

  /* No Route Found  */
  router.use(noRouteFoundController);

  app.use(router);
};
