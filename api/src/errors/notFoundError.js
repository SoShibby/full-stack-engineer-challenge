import RethrowableError from './rethrowableError';

class NotFoundError extends RethrowableError {
}

export default NotFoundError;
