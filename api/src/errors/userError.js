import RethrowableError from './rethrowableError';

class UserError extends RethrowableError {
}

export default UserError;
