import { appLogger as log } from './loggers';
import { prettyPrint } from './utils/jsonUtil';

export default {
  ok: (res, obj) => {
    // The reason that we want to wrap the response obj instead of sending it directly,
    // is that we might want to send back more data in the root object in the
    // future and to not lose that ability we send the obj in its own field
    // called result.
    const body = {
      result: obj,
    };

    log.debug(`Sending response 200 with body ${prettyPrint(body)}`);
    res.send(body);
  },
  error: (res, message, statusCode) => {
    const status = statusCode || 500;
    const body = {
      error: {
        message,
      },
    };

    log.error(`Sending response ${status} with body: ${prettyPrint(body)}`);
    res.status(status).send(body);
  },
};
