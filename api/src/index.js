import server from './server';
import { appLogger as log } from './loggers';
import config from './config';
import * as db from './database/db';

// Print the application config. This will be helpful when we need to debug the application.
log.info(`Starting application with config: ${config.prettyPrint()}`);

server.init();

db.connect().catch((error) => {
  log.error(`Failed to connect to database. ${error} ${error.stack}`);
});
