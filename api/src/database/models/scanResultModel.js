import { model } from 'mongoose';
import scanResultSchema from '../schemas/scanResultSchema';

export default model('scanResult', scanResultSchema);
