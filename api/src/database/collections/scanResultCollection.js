import ScanResultModel from '../models/scanResultModel';
import { appLogger as log } from '../../loggers';

export default {
  getAllScanResults: () => ScanResultModel.find().exec(),
  getScanResult: (id) => ScanResultModel.findById({ _id: id }),
  addScanResult: async (scanResult) => {
    log.info('Storing scan result in collection.');
    await new ScanResultModel(scanResult).save();
    log.info('Successfully saved scan result.');
  },
};
