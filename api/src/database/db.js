import mongoose from 'mongoose';
import config from '../config';
import { appLogger as log } from '../loggers';

const connect = async () => {
  const host = config.get('mongodb:host');
  const db = config.get('mongodb:db');

  log.info(`Connecting to mongodb at "${host}" with database name "${db}".`);

  // Connect to mongodb.
  await mongoose.connect(`${host}/${db}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  // Log any error that occurs after the initial connection https://mongoosejs.com/docs/connections.html.
  mongoose.connection.on('error', (err) => {
    log.error(`An unexpected error occurred in Mongodb connectiong. ${err}`);
  });

  log.info('Successfully connected to mongodb.');
};

const disconnect = async () => {
  mongoose.connection.close();
};

export {
  connect,
  disconnect,
};
