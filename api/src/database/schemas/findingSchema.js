import { Schema } from 'mongoose';
import location from './locationSchema';
import metadata from './metadataSchema';

export default new Schema({
  type: {
    type: String,
    required: true,
  },
  ruleId: {
    type: String,
    required: true,
  },
  location: {
    type: location,
    required: true,
  },
  metadata: {
    type: metadata,
    required: true,
  },
});
