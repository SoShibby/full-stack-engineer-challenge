import { Schema } from 'mongoose';

export default new Schema({
  line: {
    type: Number,
    min: 0,
    required: true,
  },
});
