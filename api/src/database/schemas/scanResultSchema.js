import { Schema } from 'mongoose';
import finding from './findingSchema';

export default new Schema({
  status: {
    type: String,
    enum: ['Queued', 'In Progress', 'Success', 'Failure'],
  },
  repositoryName: {
    type: String,
    required: true,
  },
  findings: [
    {
      type: finding,
    },
  ],
  queuedAt: {
    type: Date,
  },
  scanningAt: {
    type: Date,
  },
  finishedAt: {
    type: Date,
  },
});
