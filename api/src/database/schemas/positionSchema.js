import { Schema } from 'mongoose';
import begin from './beginSchema';

export default new Schema({
  begin: {
    type: begin,
    required: true,
  },
});
