import { Schema } from 'mongoose';
import position from './positionSchema';

export default new Schema({
  path: {
    type: String,
    required: true,
  },
  positions: {
    type: position,
    required: true,
  },
});
