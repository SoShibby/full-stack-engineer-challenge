import promBundle from 'express-prom-bundle';

export default (app) => {
  app.use(promBundle({
    includeMethod: true,
    includePath: true,
  }));
};
