import cors from 'cors';
import config from '../config';

export default (app) => {
  app.use(cors({
    origin: config.get('cors:origin'),
    optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
  }));
};
