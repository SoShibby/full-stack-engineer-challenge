import rTracer from 'cls-rtracer';

export default (app) => {
  app.use(rTracer.expressMiddleware());
};
