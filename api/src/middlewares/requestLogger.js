import {
  requestLogger as requestLog,
  responseLogger as responseLog,
} from '../loggers';

export default (app) => {
  app.use((req, res, next) => {
    requestLog.log({
      level: 'info',
      req,
    });

    // To track response time
    req.startTime = new Date();

    // Proxy the real end function
    const originalResEnd = res.end;

    res.end = (chunk, encoding) => {
      // Restore the original res.end function.
      res.end = originalResEnd;
      res.end(chunk, encoding);

      responseLog.log({
        level: 'info',
        req,
        res,
        responseTime: new Date() - req.startTime,
      });
    };

    next();
  });
};
