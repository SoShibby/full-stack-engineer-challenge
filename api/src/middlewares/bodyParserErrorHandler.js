import { appLogger as log } from '../loggers';
import response from '../response';

export default (app) => {
  // NOTE: The 'next' variable is unused, but this function won't be called if we don't have it.
  // eslint-disable-next-line no-unused-vars
  app.use((error, req, res, next) => {
    log.warn(`Body parser failed to parse json body. ${error}`);
    response.error(res, `Invalid json body. ${error.message}`, 400);
  });
};
