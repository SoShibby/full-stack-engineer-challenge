import { appLogger as log } from '../loggers';
import response from '../response';
import {
  UserError,
  NotFoundError,
} from '../errors';

export default (app) => {
  // NOTE: The 'next' variable is unused, but this function won't be called if we don't have it.
  // eslint-disable-next-line no-unused-vars
  app.use((err, req, res, next) => {
    if (err instanceof UserError) {
      // If a BadRequestError is thrown in a route then this means that the error is
      // on the user side (e.g. the user sent invalid values etc).
      log.warn(`Bad request. ${err.message}. \nStacktrace: ${err.stack}`);
      response.error(res, err.message, 400);
    } else if (err instanceof NotFoundError) {
      response.error(res, err.message, 404);
    } else {
      // For all other errors thrown in a route we send back 500 internal server error.
      // Because we don't know what error this is or what caused it.
      log.error(`An unexpected error occurred. ${err.message}. \nStacktrace: ${err.stack}`);

      // Be careful not to disclose any internal information to the user.
      // As we don't know what kind of error this is.
      response.error(res, 'An unexpected error occurred');
    }
  });
};
