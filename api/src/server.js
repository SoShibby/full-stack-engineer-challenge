import http from 'http';
import express from 'express';
import config from './config';
import { appLogger as log } from './loggers';
import routes from './routes';
import * as middlewares from './middlewares';
import { onShutdown } from './shutdown';

export default {
  init: () => {
    const app = express();

    // Secure application by setting various http headers.
    middlewares.helmet(app);

    // Add request id to all incoming http requests.
    middlewares.requestId(app);

    // Log all http request and responses to winston.
    middlewares.requestLogger(app);

    // Add cors header to limit who can call the api service.
    middlewares.cors(app);

    // Collect metrics from express (such as duration and response codes).
    middlewares.prometheus(app);

    // Parse request body strings as json if Content-Type header is set to application/json.
    middlewares.bodyParser(app);

    // Handle any error thrown by the body parser.
    // So we can send back a better error message to the user.
    middlewares.bodyParserErrorHandler(app);

    // Define all endpoints.
    routes(app);

    // Handle any error that is thrown in a route and send back an error message to the user.
    middlewares.rootErrorHandler(app);

    const server = http.createServer(app);

    // Handle any unexpected errors that might occur in the http server.
    // Example of an error is "the port is already in use" error.
    server.on('error', (error) => {
      log.error(`An unexpected error occurred with the http server. ${error.message}`);
    });

    // Request timeout is how long time (in ms) we will wait for a http
    // connection to finish before killing it.
    server.setTimeout(config.get('request:timeout'));

    // Start listening for http requests.
    const port = config.get('port');
    log.info(`Start listening for http requests at port ${port}.`);
    server.listen(port);

    // Shutdown the http server when the application exits.
    onShutdown(async () => {
      log.info('Closing http server.');
      await server.close();
    });
  },
};
