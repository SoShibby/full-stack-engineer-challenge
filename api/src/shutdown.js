import { appLogger as log } from './loggers';
import config from './config';

const listeners = [];

const sleep = (ms) => new Promise((resolve) => {
  setTimeout(resolve, ms);
});

async function shutdown() {
  log.info('Starting shutdown process');

  // When Docker shutsdown a container it changes it status to unhealthy which stop routing
  // traffic to this container and instead route them to other health containers.
  // But we can still have active http connection and we must therfore wait until
  // all connections are finished, otherwise the client will get a connection error.
  // Here we are waiting for the amount of time specified in the request timeout,
  // if we wait that long we are certain that we don't kill an active connection.
  await sleep(config.get('request:timeout'));

  try {
    // Call 'onShutdown' function on all listeners.
    await Promise.all(listeners.map((listener) => listener()));
  } catch (err) {
    log.error(`Shutdown process failed. ${err}.`);
    process.exit(1);
  }

  log.info('Shutdown process finished, shutting down...');
  process.exit(0);
}

function onShutdown(listener) {
  listeners.push(listener);
}

export {
  shutdown,
  onShutdown,
};
