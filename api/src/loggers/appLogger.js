import winston from 'winston';
import rTracer from 'cls-rtracer';
import config from '../config';

const {
  json,
  printf,
  combine,
  timestamp,
} = winston.format;

// Add request id to the log object.
const addRequestId = winston.format((info) => {
  const requestId = rTracer.id();

  if (requestId) {
    return {
      request_id: requestId,
      ...info,
    };
  }

  return info;
});

// Create a logger that outputs to the console.
const logger = winston.createLogger({
  level: config.get('log:level'),
  transports: [new winston.transports.Console()],
});

const stringFormat = printf((info) => (info.request_id
  ? `${info.timestamp} [${info.level}] [${info.request_id}]: ${info.message}`
  : `${info.timestamp} [${info.level}]: ${info.message}`
));

if (process.env.NODE_ENV === 'production') {
  // Output the log object as json when in production.
  // It will be easier to parse if we are using a centralized logging system.
  logger.format = combine(
    addRequestId(),
    json(),
  );
} else {
  // Output the log object as a string when in dev mode.
  // This is so the output will be easier to read for the developer.
  logger.format = combine(
    timestamp(),
    addRequestId(),
    stringFormat,
  );
}

export default logger;
