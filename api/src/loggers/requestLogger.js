import winston from 'winston';
import rTracer from 'cls-rtracer';

const {
  json,
  printf,
  combine,
  timestamp,
} = winston.format;

// Add request id to the log object.
const addRequestId = winston.format((info) => {
  const requestId = rTracer.id();

  if (requestId) {
    return {
      request_id: requestId,
      ...info,
    };
  }

  return info;
});

const getIP = (req) => req.ip
  // eslint-disable-next-line no-underscore-dangle
  || req._remoteAddress
  || (req.connection && req.connection.remoteAddress)
  || undefined;

// Add message to the log object.
const addMessage = winston.format((info) => ({
  message: `Request ${info.req.method} ${info.req.originalUrl} ${info.req.headers['user-agent']} ${getIP(info.req)}`,
  ...info,
}));

// Create a logger that outputs to the console.
const logger = winston.createLogger({
  transports: [new winston.transports.Console()],
});

const stringFormat = printf((info) => `${info.timestamp} [${info.level}] [${info.request_id}]: ${info.message}`);

if (process.env.NODE_ENV === 'production') {
  // Output the log object as json when in production.
  // It will be easier to parse if we are using a centralized logging system.
  logger.format = combine(
    addRequestId(),
    addMessage(),
    json(),
  );
} else {
  // Output the log object as a string when in dev mode.
  // This is so the output will be easier to read for the developer.
  logger.format = combine(
    timestamp(),
    addRequestId(),
    addMessage(),
    stringFormat,
  );
}

export default logger;
