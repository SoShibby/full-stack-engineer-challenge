import nconf from 'nconf';
import { prettyPrint } from './utils/jsonUtil';

nconf.env({
  parseValues: true,
  separator: '__',
  whitelist: [
    'port',
    'mongodb__host',
    'mongodb__db',
    'cors__origin',
    'request__timeout',
    'log__level',
  ],
});

nconf.defaults({
  port: 8090,
  log: {
    level: 'info',
  },
  mongodb: {
    // Don't set default values for the database.
    // We don't want to accidentally connect to the wrong database.
    host: undefined,
    db: undefined,
  },
  cors: {
    origin: '*',
  },
  request: {
    timeout: 30000,
  },
});

nconf.required([
  'port',
  'mongodb:host',
  'mongodb:db',
  'cors:origin',
  'request:timeout',
  'log:level',
]);

nconf.prettyPrint = () => prettyPrint(nconf.get());

export default nconf;
