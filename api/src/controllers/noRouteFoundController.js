import { NotFoundError } from '../errors';

export default () => {
  throw new NotFoundError('Endpoint does not exist');
};
