import response from '../response';
import collection from '../database/collections/scanResultCollection';
import {
  UserError,
  NotFoundError,
} from '../errors';
import { ScanResultValidator } from '../validators';

const controller = {
  getScanResults: async (req, res) => {
    const scanResults = await collection.getAllScanResults();
    response.ok(res, scanResults);
  },
  getScanResult: async (req, res) => {
    const { id } = req.params;

    if (!id) {
      throw new UserError('No id parameter provided');
    }

    const scanResult = await collection.getScanResult(id);

    if (!scanResult) {
      throw new NotFoundError(`No scan result exists with the id '${id}'.`);
    }

    response.ok(res, scanResult);
  },
  postScanResult: async (req, res) => {
    const scanResult = req.body;

    try {
      await ScanResultValidator.validate(scanResult);
    } catch (error) {
      // If the validation function throws an error we rethrow it as a UserError,
      // as the error is because the user posted incorrect values.
      throw new UserError(error.message, error);
    }

    // Add the scan result to the database.
    await collection.addScanResult(scanResult);

    response.ok(res, 'Successfully saved scan result.');
  },
};

export default controller;
