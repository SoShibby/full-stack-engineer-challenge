import controller from '../scanResultController';
import collection from '../../database/collections/scanResultCollection';

jest.mock('nconf');
jest.mock('../../database/collections/scanResultCollection');

const { describe, test, expect } = global;

const validScanResult = {
  id: '123',
  status: 'Queued',
  repositoryName: 'RepoA',
  queuedAt: '2015-06-22T13:17:21+0000',
  scanningAt: '2015-06-22T13:17:21+0000',
  finishedAt: '2015-06-22T13:17:21+0000',
  findings: [
    {
      type: 'sast',
      ruleId: 'G402',
      location: {
        path: 'connectors/apigateway.go',
        positions: {
          begin: {
            line: 60,
          },
        },
      },
      metadata: {
        description: 'TLS InsecureSkipVerify set true.',
        severity: 'HIGH',
      },
    },
    {
      type: 'sast',
      ruleId: 'G404',
      location: {
        path: 'util/util.go',
        positions: {
          begin: {
            line: 32,
          },
        },
      },
      metadata: {
        description: 'Use of weak random number generator (math/rand instead of crypto/rand)',
        severity: 'HIGH',
      },
    },
  ],
};

describe('scanResultController', () => {
  test('get all scan results', async () => {
    const scanResults = [validScanResult];
    const res = {
      send: jest.fn(),
    };

    collection.getAllScanResults.mockResolvedValue(scanResults);

    await controller.getScanResults(null, res);
    expect(res.send).toBeCalledWith({
      result: scanResults,
    });
  });

  test('get a single scan result', async () => {
    const scanResult = validScanResult;
    const req = {
      params: {
        id: 'test',
      },
    };
    const res = {
      send: jest.fn(),
    };

    collection.getScanResult.mockResolvedValue(scanResult);

    await controller.getScanResult(req, res);
    expect(res.send).toBeCalledWith({
      result: scanResult,
    });
  });

  test('get a single scan result that doesn\'t exists', async () => {
    expect.assertions(1);

    const req = {
      params: {
        id: "this id doesn't exist",
      },
    };

    // When a scan result isn't found in the database a null value is returned instead.
    collection.getScanResult.mockResolvedValue(null);

    try {
      await controller.getScanResult(req, null);
    } catch (err) {
      expect(err.message).toBe("No scan result exists with the id 'this id doesn't exist'.");
    }
  });

  test('post valid scan result', async () => {
    const req = {
      body: validScanResult,
    };
    const res = {
      send: jest.fn(),
    };

    collection.addScanResult.mockResolvedValue();

    await controller.postScanResult(req, res);

    expect(res.send).toBeCalledWith({
      result: 'Successfully saved scan result.',
    });
  });

  test('post empty scan result', async () => {
    const req = {
      body: {},
    };
    const res = {
      send: jest.fn(),
    };

    collection.addScanResult.mockResolvedValue();

    expect.assertions(1);

    try {
      await controller.postScanResult(req, res);
    } catch (error) {
      expect(error.message).toBe('scanResult validation failed: repositoryName: Path `repositoryName` is required.');
    }
  });

  test('post null scan result', async () => {
    const req = {
      body: null,
    };
    const res = {
      send: jest.fn(),
    };

    collection.addScanResult.mockResolvedValue();

    expect.assertions(1);

    try {
      await controller.postScanResult(req, res);
    } catch (error) {
      expect(error.message).toBe('scanResult validation failed: repositoryName: Path `repositoryName` is required.');
    }
  });
});
