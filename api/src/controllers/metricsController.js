import Prometheus from 'prom-client';
import { appLogger as log } from '../loggers';
import { onShutdown } from '../shutdown';

const metricsInterval = Prometheus.collectDefaultMetrics();

onShutdown(async () => {
  log.info('Stopping Prometheus collector.');
  clearInterval(metricsInterval);
});

export default {
  getMetrics: (req, res) => {
    res.set('Content-Type', Prometheus.register.contentType);
    res.end(Prometheus.register.metrics());
  },
};
