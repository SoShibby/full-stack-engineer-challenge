import {
  HealthChecker,
  LivenessEndpoint,
  ReadinessEndpoint,
  HealthEndpoint,
  ShutdownCheck,
} from '@cloudnative/health-connect';
import { shutdown } from '../shutdown';

const healthcheck = new HealthChecker();

// Start the shutdown process when the process receives a SIGTERM.
// The 'shutdown' argument will be invoked when we get the SIGTERM signal.
healthcheck.registerShutdownCheck(new ShutdownCheck('Application is shutting down', shutdown));

export default {
  live: () => LivenessEndpoint(healthcheck),
  ready: () => ReadinessEndpoint(healthcheck),
  health: () => HealthEndpoint(healthcheck),
};
