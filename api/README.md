# API Service

## Install dependencies

Run the following command to install all the necessary dependencies:

```sh
npm install
```

## Start

Run the following command to start the application in development mode:

```sh
npm start
```

Your application is now available at `http://localhost:8090` by default.

Note: you might need to change the url to your mongodb server in the package.json file to point to your server.

## Lint

Run the following command to lint the application:

```sh
npm run lint
```

## Test

Run the following command to run all tests:

```sh
npm run test
```

## Build

Run the following command to build the application:

```sh
npm run build
```

