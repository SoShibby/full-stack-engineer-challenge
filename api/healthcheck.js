const http = require('http');

const options = {
  host: '0.0.0.0',
  port: 8090,
  path: '/health',
  timeout: 2000,
};

const healthCheck = http.request(options, (res) => {
  // eslint-disable-next-line no-console
  console.log(`Healthcheck status: ${res.statusCode}`);

  if (res.statusCode === 200) {
    process.exit(0);
  } else {
    process.exit(1);
  }
});

healthCheck.on('error', (err) => {
  // eslint-disable-next-line no-console
  console.error('An error occurred during healthcheck.', err);
  process.exit(1);
});

healthCheck.end();
