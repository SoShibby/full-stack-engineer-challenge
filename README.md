# Full Stack Engineer Challenge

> This repository contains the challenge for full stack engineers.

- [Devops Exercise](#devops-exercise)
- [Backend Exercise](#backend-exercise)
- [Frontend Exercise](#frontend-exercise)

**Note:** Please don't fork this repository, or create a pull request against it. Otherwise other applicants may take inspiration from it. Once the coding challenge is completed, you can submit it via this [Google form](https://forms.gle/f2hekWPJqee6htH28).

## Devops Exercise

The backend and frontend exercises should be contained in one repository (monorepo), with the following directory layout:

```bash
├── api
├── dashboard
├── docker-compose.yml
└── README.md # documentation for this repo
```

The `api` and `dashboard` directories should have their own simple `Dockerfile`, so that they can be built and run locally.

The `docker-compose.yml` file should define both the `api` and `dashboard` as services, which will be automatically built and started upon calling `docker-compose up`.

__Things you don’t have to worry about:__

- CI configuration / Deployment
- Secret Management

## Backend Exercise

The project should be made available in the `api` directory with meaningful commit messages. Use Node.js and any framework, if you want to use one.

Implement a **simple REST API** to create and retrieve a Security Scan Result (“Result”). The Result entity should have the following properties:

- Id: (unique)
- Status: (Queued, In Progress, Success, Failure)
- RepositoryName: String
- Findings: JSONB, see [example](example-findings.json)
- QueuedAt: timestamp
- ScanningAt: timestamp
- FinishedAt: timestamp

The Result entity should be stored in a database (any). Wherever you’d have to add something that you feel requires product subscriptions or significant extra time, just mention it in the README.md file.

__Things you don’t have to worry about:__

- CI configuration / Deployment
- APM
- Authentication / Authorisation / Auditing

## Frontend Exercise

The project should be made available in a the `dashboard` directory with meaningful commit messages. You should use ReactJS and JavaScript.

Implement a simple dashboard that has three screens:

1. Display a form that allows submitting a scan result
2. Displays a list of security scan results.
The list should display the name of the repository, the status of the scan, and the according timestamp.
If the Scan Report contains findings, display the number in a badge.
3. Displays the findings for a selected security scan. 

Once a scan report was selected, display a list of findings:

- RuleId of the finding
- Description of the finding
- Severity of finding
- Path name and line of code of the finding

The application is fairly simple, so you might not want to use any state management libraries – in this case please make sure your state management is clean, simple and easy to test. Wherever you’d have to add something that you feel requires product subscriptions (e.g. Logging 3rd party service) or significant extra time, just mention it in the README.md file.

__Things you don’t have to worry about:__

- Making it super pretty: The UI should be clean and properly aligned however it does not
need any extraneous CSS and/or animations. You can use any UI framework you like.
- CI configuration / Deployment

## Build And Start Containers

Run the following command in the root folder to build and start all containers.

```sh
docker-compose up --build
```

Open the following url `http://YOUR_DOMAIN_NAME_OR_IP:3000` in your browser to view the application.

Note: The default hostname is set to 192.168.99.100, if your ip is different then you need to change this ip by either changing it in the docker-compose file or set the following environment variables:

```sh
export SCAN_RESULT_SERVICE_HOST=http://YOUR_DOMAIN_NAME_OR_IP
export CORS_ORIGIN=http://YOUR_DOMAIN_NAME_OR_IP:3000 # Note that there is a port number at the end here that should point to the same port as the dashboard container.
```

## TODO
- Setup backup and restore process for Mongodb.
- Send logs to a centralized location using something like EFK. So we can easily view and filter logs.
- Setup Prometheus and Grafana for fetching and visualizing application metrics.
- Setup Prometheus alerts for the application, so we can be notified when something goes wrong.
- Setup Jaeger for request tracing.
- Look into the resource limit constraints that is set on the Mongodb and the API application, 
  so they are configured correctly. We should stress test the application to see how much,
  memory and cpu they use under load.
- Add authentication and authorization.
- Automatically warn if we are using dependencies with known vulnerabilities.
- Send notifications if newer dependencies are available.
- Add Sentry for error reporting (https://sentry.io).
- Setup CI/CD pipelines.

